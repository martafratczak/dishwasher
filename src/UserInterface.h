#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <iostream>
#include <string>

#include "Dishwasher.h"

using namespace std;

class UserInterface {

private:
    shared_ptr<Dishwasher> dishwasher;

    int getNumberFromUser(int from, int to);
    int displayOptions();
    int askUserToChooseProgram();

public:
    const shared_ptr<Dishwasher> &getDishwasher() const;
    void setDishwasher(const shared_ptr<Dishwasher> &dishwasher);

    void handleDishwasher();
};

#endif
