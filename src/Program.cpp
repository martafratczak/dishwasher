#include <iostream>
#include <thread>
#include <sstream>
#include <cmath>

#include "Program.h"

using namespace std;

Program::Program(int id, string n, int d, int t, float e) {
    this->id = id;
    this->name = n;
    this->duration = d;
    this->temperature = t;
    this->energyRequired = e;
    this->backgroundThread = thread(&Program::executionHandler, this);
}

int Program::getId() const {
    return id;
}

void Program::setId(int id) {
    Program::id = id;
}

const string &Program::getName() const {
    return name;
}

void Program::setName(const string &name) {
    Program::name = name;
}

int Program::getDuration() const {
    return duration;
}

void Program::setDuration(int duration) {
    Program::duration = duration;
}

int Program::getTemperature() const {
    return temperature;
}

void Program::setTemperature(int temperature) {
    Program::temperature = temperature;
}

int Program::getEnergyRequired() const {
    return energyRequired;
}

void Program::setEnergyRequired(int energyRequired) {
    Program::energyRequired = energyRequired;
}

const shared_ptr<Dishwasher> &Program::getDishwasher() const {
    return dishwasher;
}

void Program::setDishwasher(const shared_ptr<Dishwasher> &dishwasher) {
    Program::dishwasher = dishwasher;
}

int Program::getTimeElapsed() const {
    return timeElapsed;
}

void Program::executionHandler() {
    while (true) {
        this_thread::sleep_for(chrono::milliseconds(100));
        if (state == State::running) timeElapsed += 1;
        if (timeElapsed >= duration) {
            state = State::finished;
            dishwasher->setState(Dishwasher::finished);
            if(dishwasher->getState() == Dishwasher::State::turnedOff) break;
        }
    }
}

void Program::start() {
    state = State::running;
    timeElapsed = 0;
}

void Program::stop() {
    state = State::ready;
    timeElapsed = 0;
}

void Program::pause() {
    state = State::paused;
}

void Program::unpause() {
    state = State::running;
}

string Program::getInfo() {
    stringstream ss;
    ss    << id << ". Prog.: " << name << ";     Dur.: " << duration << " min;     Temp.: "
          << temperature << "*C;     Enr. Req.: " << energyRequired << "kW" << endl;
    return ss.str();
}

string Program::getProgress() {
    stringstream ss;
    ss << getTimeElapsed() << "/" << getDuration();
    return ss.str();
}

string Program::getProgressPercent() {
    stringstream ss;
    ss << "(" << ceil(100.0*getTimeElapsed()/getDuration()) << "%)";
    return ss.str();
}
