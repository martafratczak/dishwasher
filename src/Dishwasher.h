#ifndef DISHWASHER_H
#define DISHWASHER_H

#include <vector>
#include <memory>

using namespace std;

class Program;

class Dishwasher {

public:
    enum State { turnedOff, turnedOn, chosen, running, paused, finished};

private:
    vector<shared_ptr<Program>> programs;
    shared_ptr<Program> chosenProgram;

    State state;

public:
    void readProgramsFromFile();

    void turnOn();
    void turnOff();

    bool chooseProgram(int);
    void startProgram();
    void stopProgram();
    void pauseProgram();
    void unpauseProgram();

    int getProgramsCount();

    State getState() const;
    void setState(State state);

    bool isTurnedOn() const;

    bool isProgramRunning() const;
    bool isProgramPaused() const;
    bool isProgramChosen() const;

    void showPrograms();
    void showStatus();
};

#endif
