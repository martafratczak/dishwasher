#include <iostream>
#include <fstream>
#include <future>

#include "Dishwasher.h"
#include "Program.h"

using namespace std;

void Dishwasher::readProgramsFromFile() {
    ifstream inputFile;
    inputFile.open("../programs.txt");
    string tempLine = "";

    while (getline(inputFile, tempLine)) {
        int i = 0; //current line sign
        string id = "", name = "", duration = "", temperature = "", energyRequired = "";

        while (i < tempLine.length()) {
            id += tempLine[i]; i++;
            if ((char)tempLine[i] == 58) { i++; break; }
        }

        while (i < tempLine.length()) {
            name += tempLine[i]; i++;
            if ((char)tempLine[i] == 58) { i++; break; }
        }

        while (i < tempLine.length()) {
            duration += tempLine[i]; i++;
            if ((char)tempLine[i] == 58) { i++; break; }
        }

        while (i < tempLine.length()) {
            temperature += tempLine[i]; i++;
            if ((char)tempLine[i] == 58) { i++; break; }
        }

        while (i < tempLine.length()) {
            energyRequired += tempLine[i]; i++;
        }
        shared_ptr<Program> program(new Program(stoi(id), name, stoi(duration), stoi(temperature), stof(energyRequired)));
        program->setDishwasher(shared_ptr<Dishwasher>(this));
        this->programs.push_back(program);
    }
    inputFile.close();
}

void Dishwasher::turnOn() {
    state = State::turnedOn;
    cout << "Dishwasher is now on;" << endl;
}

void Dishwasher::turnOff() {
    state = State::turnedOff;
    cout << "Dishwasher is now off;" << endl;
}

bool Dishwasher::chooseProgram(int id) {
    for (auto & program : programs)
        if (program->getId() == id){
            chosenProgram = program;
            state = State::chosen;
            return true;
        }
    return false;
}

void Dishwasher::startProgram() {
    if (isProgramChosen()) {
        state = State::running;
        chosenProgram->start();
    }
}

void Dishwasher::stopProgram() {
    if (isProgramChosen()) {
        state = State::chosen;
        chosenProgram->stop();
    }
}

void Dishwasher::pauseProgram() {
    if (isProgramRunning()) {
        state = State::paused;
        chosenProgram->pause();
    }
}

void Dishwasher::unpauseProgram() {
    if (!isProgramRunning()) {
        state = State::running;
        chosenProgram->unpause();
    }
}

int Dishwasher::getProgramsCount() {
    return programs.size();
}

Dishwasher::State Dishwasher::getState() const {
    return state;
}

void Dishwasher::setState(Dishwasher::State state) {
    Dishwasher::state = state;
}

bool Dishwasher::isTurnedOn() const {
    return state != State::turnedOff;
}

bool Dishwasher::isProgramRunning() const {
    return state == State::running;
}

bool Dishwasher::isProgramPaused() const {
    return state == State::paused;
}

bool Dishwasher::isProgramChosen() const {
    return state == State::chosen || state == State::paused || state == State::running || state == State::finished;
}

void Dishwasher::showPrograms() {
    for (int i = 0; i < programs.size(); i++)
        cout << programs[i]->getInfo();
}

void Dishwasher::showStatus() {
    cout << endl << "-------------------------------------------------" << endl;
    cout << "Dishwasher is ready!" << endl;
    cout << "Available programs: " << getProgramsCount() << endl;

    if(isProgramChosen()) {
        cout << "Chosen program: " << chosenProgram->getInfo() << endl;
    } else {
        cout << "Chosen program: none" << endl << endl;
    }

    if (isProgramRunning())
            cout << "Program is running for " << chosenProgram->getProgress() << " " << chosenProgram->getProgressPercent() << endl;
    else if (chosenProgram != nullptr && chosenProgram->getTimeElapsed() == chosenProgram->getDuration())
        cout << "Program has finished" << endl;
    else if (isProgramPaused())
        cout << "Program is paused. Progress: " << chosenProgram->getProgress() << " " << chosenProgram->getProgressPercent() << endl;
    else
        cout << "Program has not been started yet" << endl;
}


