#include "UserInterface.h"

const shared_ptr<Dishwasher> &UserInterface::getDishwasher() const {
    return dishwasher;
}

void UserInterface::setDishwasher(const shared_ptr<Dishwasher> &dishwasher) {
    UserInterface::dishwasher = dishwasher;
}

int UserInterface::getNumberFromUser(int from, int to) {
    string input; int inputInt;
    while (true){
        getline(cin, input);
        inputInt = atoi(input.c_str());
        if (inputInt < from || inputInt > to) { continue; }
        break;
    }
    return inputInt;
}

int UserInterface::displayOptions() {
    dishwasher->showStatus();
    cout << endl;
    cout << "1. Choose program" << endl;
    cout << "2. Start/stop active program" << endl;
    cout << "3. Pause/unpause active program" << endl;
    cout << "4. Turn off the machine" << endl;
    cout << "5. REFRESH MENU" << endl;
    cout << "TYPE NUMBER: ";

    return getNumberFromUser(1, 5);
}

int UserInterface::askUserToChooseProgram() {
    dishwasher->showPrograms();
    cout << "TYPE NUMBER: ";

    return getNumberFromUser(1, dishwasher->getProgramsCount() + 1);
}

void UserInterface::handleDishwasher() {
    while (dishwasher->isTurnedOn()) {
        switch (displayOptions()) {
            case 1: //choose program
                dishwasher->chooseProgram(askUserToChooseProgram());
                break;
            case 2: //start/stop active program
                if (dishwasher->isProgramRunning()) dishwasher->stopProgram();
                else if(dishwasher->isProgramPaused()) ;
                else dishwasher->startProgram();
                break;
            case 3: //pause/unpause active program
                if (dishwasher->isProgramPaused()) dishwasher->unpauseProgram();
                else dishwasher->pauseProgram();
                break;
            case 4: //turn off the machine
                dishwasher->turnOff();
                break;
            case 5: //refresh
                continue;
            default:
                continue;
        }
    }
}
