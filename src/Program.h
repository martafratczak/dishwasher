#ifndef PROGRAM_H
#define PROGRAM_H

#include <string>
#include "Dishwasher.h"

using namespace std;

class Program {

private:
    int id;
    string name;
    int duration;
    int temperature;
    float energyRequired;

    enum State { ready, running, paused, finished };
    State state;

    int timeElapsed{};

    shared_ptr<Dishwasher> dishwasher;

    thread backgroundThread;

public:
    Program(int, string, int, int, float);

    int getId() const;
    void setId(int id);

    const string &getName() const;
    void setName(const string &name);

    int getDuration() const;
    void setDuration(int duration);

    int getTemperature() const;
    void setTemperature(int temperature);

    int getEnergyRequired() const;
    void setEnergyRequired(int energyRequired);

    const shared_ptr<Dishwasher> &getDishwasher() const;
    void setDishwasher(const shared_ptr<Dishwasher> &dishwasher);

    int getTimeElapsed() const;
    void executionHandler();

    void start();
    void stop();
    void pause();
    void unpause();

    string getInfo();
    string getProgress();
    string getProgressPercent();
};

#endif
