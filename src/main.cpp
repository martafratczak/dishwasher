#include <memory>
#include "Dishwasher.h"
#include "UserInterface.h"

using namespace std;

int main (int argc, char *argv[]) {

    shared_ptr<Dishwasher> dishwasher = make_shared<Dishwasher>();

    dishwasher->turnOn();
    dishwasher->readProgramsFromFile();

    shared_ptr<UserInterface> userInterface = make_shared<UserInterface>();
    userInterface->setDishwasher(dishwasher);

    userInterface->handleDishwasher();

    return 0;
}

/*
 * 1.9.  TaskWith the use of tools and methoods provided in this instruction createa  software  for  virtual  dishwasher.
 * At  the  start  of  the  software  cycle,  usermust be informed about:  the current washing program,  the length
 * of cur-rent program, water temperature for current program, enegry consumptionfor current program.  Virtual dishwasher
 * should have at least four differentwashing programs.  Each washing program should use parameters as:  name,length,
 * water temperature, energy consumption.  Values of this parameters should be stored in an external file.  User should
 * be able to choose a washingprogram, star the washing cycle, pause the washing cycle, stop the washingcycle.
 * */

