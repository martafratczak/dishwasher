cmake_minimum_required(VERSION 3.6)
project(dishwasher)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-std=c++11 -pthread")

#find_package (Threads)



set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/out)

include_directories(include)
set(SOURCES

#        src/system/SimulationLogic.cpp
#        src/system/UserInterface.cpp
#
#        src/bank/Account.cpp
#        src/bank/JuniorAccount.cpp
#        src/bank/PersonalAccount.cpp
#        src/bank/StudentsAccount.cpp
#
#        src/bank/Bank.cpp
#
#        src/bank/Transfer.cpp
        src/Program.cpp src/Program.h src/Dishwasher.cpp src/Dishwasher.h src/UserInterface.cpp src/UserInterface.h)

add_executable(dishwasher src/main.cpp ${SOURCES})

